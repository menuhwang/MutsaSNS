package com.likelion.mutsasns.observer.handler;

import com.likelion.mutsasns.observer.events.AlarmEvent;
import com.likelion.mutsasns.repository.AlarmRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class AlarmEventHandler {
    private final AlarmRepository alarmRepository;

    @Async
    @EventListener
    public void createAlarm(AlarmEvent e) {
        log.info("알람 생성 type:{}, from:{}, to:{}", e.getAlarm().getAlarmType(), e.getAlarm().getFromUser().getUsername(), e.getAlarm().getTargetUser().getUsername());
        alarmRepository.save(e.getAlarm());
        log.info("알람 생성 완료.");
    }
}
